package main

import (
	"context"
	"gitlab.com/go-yp/grpc-counter/models/protos/services/counter"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
	"log"
	"net/http"
	"sync/atomic"

	"github.com/improbable-eng/grpc-web/go/grpcweb"
	"google.golang.org/grpc"
)

type counterServer struct {
	count uint32
}

func (s *counterServer) CountSomeMethodName(context.Context, *counter.Empty) (*counter.Response, error) {
	var newCount = atomic.AddUint32(&s.count, 1)

	return &counter.Response{
		Count: newCount,
	}, nil
}

var (
	mainServer counter.CounterSomeServiceNameServer = new(counterServer)
)

func main() {
	go func() {
		grpcServer := grpc.NewServer()
		grpcWebServer := grpcweb.WrapServer(grpcServer)

		counter.RegisterCounterSomeServiceNameServer(grpcServer, mainServer)

		var handler = h2c.NewHandler(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-User-Agent, X-Grpc-Web")

			grpcWebServer.ServeHTTP(w, r)
		}), new(http2.Server))

		err := http.ListenAndServe(":50551", handler)
		if err != nil {
			log.Fatal(err)
		}
	}()

	http.Handle("/", http.FileServer(http.Dir("./public")))

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
