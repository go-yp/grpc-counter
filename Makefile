protoc-install:
	PROTOC_ZIP=protoc-3.11.4-linux-x86_64.zip
	curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/$PROTOC_ZIP
	sudo unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
	sudo unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
	rm -f $PROTOC_ZIP

protoc-grpc-web-install:
	curl -sSL https://github.com/grpc/grpc-web/releases/download/1.0.7/protoc-gen-grpc-web-1.0.7-linux-x86_64 -o /usr/local/bin/protoc-gen-grpc-web
	chmod +x /usr/local/bin/protoc-gen-grpc-web

proto-client:
	mkdir -p ./client
	protoc -I . protos/services/counter/*.proto --js_out=import_style=commonjs,binary:client --grpc-web_out=import_style=commonjs,mode=grpcwebtext:client

proto-server:
	mkdir -p ./models
	protoc -I . protos/services/counter/*.proto --go_out=plugins=grpc:models

test: proto-server proto-client
	npm i
	npm run build
	go run main.go &
	browse http://localhost:8080/