const {Empty, Response} = require("./protos/services/counter/counter_pb");
const {CounterSomeServiceNameClient} = require("./protos/services/counter/counter_grpc_web_pb");

const app = new CounterSomeServiceNameClient("http://localhost:50551");

var request = new Empty();

app.countSomeMethodName(request, {}, (err, response) => {
    if (err) {
        console.error(err);

        return;
    }

    /** @type Response response */

    document.getElementById("js-counter").innerHTML = response.getCount();
});
