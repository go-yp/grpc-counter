module.exports = {
    context: __dirname,

    entry: {
        app: './client/app'
    },

    output: {
        path: __dirname + '/public/js',
        filename: '[name].js'
    },
};